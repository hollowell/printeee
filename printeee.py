import schedule
import time
import shutil
import os
import subprocess

bluetooth_address = "" #ie "00:1F:47:37:7C:02"
obex = ["obexftp", "--nopath", "--noconn", "--uuid", "none", "--bluetooth", bluetooth_address, "--channel", "4", "-p"]

source_dir = "./source"
destination_dir = "./destination"

def proccess(file):
    try:
        print("sending " + file)
        result = subprocess.check_output(obex + [file])
        shutil.move(source_dir + "/" + file, destination_dir + "/" + file)
        pass
    except Exception as e:
        print e
        raise

def job():
    files = next(os.walk(source_dir))[2]
    files.sort(key=lambda file: os.stat(source_dir + "/" + file).st_mtime)
    proccess(files[0]) if files else None

schedule.every(5).seconds.do(job)

while True:
    schedule.run_pending()
    time.sleep(1)
